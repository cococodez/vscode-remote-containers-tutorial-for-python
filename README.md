# Vscode Remote Containers Tutorial for Python

This tutorial will walk through the basics of working with vscode remote - containers and python. By the end of this tutorial, you will have a full fledged remote development environment running inside a Docker container.

## Docker as a Development Environment

One of the biggest challenges for any developer or team is having consistent development and testing environment. Docker provides a solution to this problem by isolating environments into containers. Until recently, actually developing inside docker containers while doable, was often a pain to setup and the overall experience left something to be desired. Enter [Visual Studio Code Remote - Containers](https://code.visualstudio.com/docs/remote/containers).

According to the docs:

 "Visual Studio Code Remote - Containers extension lets you use a Docker container as a full-featured development environment. It allows you to open any folder inside (or mounted into) a container and take advantage of VS Code's full feature set... This lets VS Code provide a local-quality development experience — including full IntelliSense (completions), code navigation, and debugging — regardless of where your tools (or code) is located"

 Let's see if it lives up to the hype.

### Setting Up Vscode

Visual Studio Code is an open source, cross-platform, lightweight but powerful IDE. Vscode's power comes from it rich extensions ecosystem, providing support for almost every major programming language and huge range of features. Visual Studio Code Remote - Containers is an extension that will allow us to develop inside a container but with all the convivence and features of local development environment.

To get started, follow this [guide](https://code.visualstudio.com/docs/remote/containers#_getting-started) to get all the prerequistes installed and configured (vscode, docker, etc...). Make sure you have the May 2019 release or later, if not update to the latest version.

I highly recommend installing the [Docker](https://marketplace.visualstudio.com/items?itemName=PeterJausovec.vscode-docker) extension. It makes interacting with docker within vscode easy and one less terminal I need to have open.

### Setting up the Project

First, we're going to need some code to work with. We're going to build a simple Flask hello world app, using [poetry](https://poetry.eustace.io) to manage our application and development dependencies. Managing python environments can be a pain and add further complexity. By using docker we further isolate our environment and actually simplify things. There is a little bit of work to get it started, but this one time cost pays off because it allows anyone to start contributing to your project quickly.

We're also going to use the awesome [pre-commit](https://pre-commit.com/) library to enforce coding standards and style during development.

1. Clone the repo from [here](someurlhere)

2. Cd into the directory and run `code .`

3. Follow the prompt to reopen the folder in a container
    ![alt text](img/reopen-folder-in-container.png "reopen-folder-in-container")
    

And that's it! Simple, straight forward and ready to start contributing!

### Understanding .devcontainer

The `.devcontainer` is where all the magic happens. The presence of this directory with a `devcontainer.json` tells vscode to how to launch the remote development container. Let's break it down:

```json 
"name": "Python 3",
"context": "..",
"dockerFile": "Dockerfile"
```

- `context`: The context folder to build the docker image from

- `dockerFile`: The docker file used to build the image of the container

**_Note_**: Both the dockerfile and context folder are relative to the `devcontainer.json` path

```json
"appPort": [5000]
```

- `appPort`: Publishes the port of the container to localhost

```json
"runArgs": [
    "-v","/var/run/docker.sock:/var/run/docker.sock",   
    "-v", "~/.ssh:/tmp/.ssh"
    ]
```

- `runArgs`: Valid arguments of `docker run`, in this case mounting two volumes from the host into the container


```json
"postCreateCommand": "poetry install --no-interaction --no-ansi"
```

- `postCreateCommand`: A command to run after creating the container. Here we install all our python dependencies with `poetry install`

