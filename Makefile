.PHONY: precommit precommitdocker clean codemetrics docs format formatcheck lint securitycheck typecheck tests

clean:
	find . -regex $(PR) -delete
	rm -rf docs/_build
	rm -rf logs
	rm -f .coverage

codemetrics:
	test -d logs || mkdir logs
	radon cc . --min B --total-average > logs/metrics-cc.log
	radon raw . --summary > logs/metrics-raw.log

docs:
	sphinx-build -b html docs/ docs/_build
	sphinx-build -b coverage docs/ docs/_build/_coverage

format:
	black . --target-version py36 --exclude /docker

formatcheck:
	black . --check --target-version py36 --exclude /docker

lint:
	test -d logs || mkdir logs
	find . -iname "*.py" | xargs pylint > logs/pylint.log

precommit: clean tests format lint codemetrics securitycheck

securitycheck:
	test -d logs || mkdir logs
	safety check --full-report > ./logs/safety.log && bandit -f txt -o ./logs/bandit.log -r src/

tests:
	test -d logs || mkdir logs
	python -m pytest tests --verbose --junit-xml logs/unittest-results.xml